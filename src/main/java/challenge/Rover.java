package challenge;

public interface Rover {
    void processInstructions(String instructions);
}

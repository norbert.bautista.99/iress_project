package challenge;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        MarsRover rover = new MarsRover(0, 0, Direction.N);

        System.out.println("Enter rover instructions (combination of 'L', 'R', and 'M'):");
        String instructions = scanner.nextLine();

        rover.processInstructions(instructions);

        System.out.println("Rover's final position: " + rover);
    }
}

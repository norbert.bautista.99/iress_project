package challenge;

public class MarsRover implements Rover {
    private static final int gridMaxX = 5;
    private static final int gridMaxY = 5;

    private Position position;

    public MarsRover(int x, int y, Direction direction) {
        this.position = new Position(x, y, direction);
    }

    @Override
    public void processInstructions(String instructions) {
        for (char instruction : instructions.toCharArray()) {
            switch (instruction) {
                case 'L':
                    position.setDirection(position.getDirection().turnLeft());
                    break;
                case 'R':
                    position.setDirection(position.getDirection().turnRight());
                    break;
                case 'M':
                    move();
                    break;
                default:
                    System.out.println("Invalid instruction: " + instruction);
                    break;
            }
        }

    }
    private void move() {
        switch (position.getDirection()) {
            case N:
                // Move north if not at the northern edge
                if (position.getY() < gridMaxY) {
                    position.setY(position.getY() + 1);
                }
                break;
            case E:
                // Move east if not at the eastern edge
                if (position.getX() < gridMaxX) {
                    position.setX(position.getX() + 1);
                }
                break;
            case S:
                // Move south if not at the southern edge (assuming y decreases to the south)
                if (position.getY() > 0) {
                    position.setY(position.getY() - 1);
                }
                break;
            case W:
                // Move west if not at the western edge
                if (position.getX() > 0) {
                    position.setX(position.getX() - 1);
                }
                break;
        }
    }
    @Override
    public String toString() {
        return position.getX() + ", " + position.getY() + ", " + position.getDirection();
    }
}

package challenge;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PositionTest {
    @Test
    void testInitialPosition() {
        Position position = new Position(0, 0, Direction.N);
        assertAll("Initial Position",
                () -> assertEquals(0, position.getX()),
                () -> assertEquals(0, position.getY()),
                () -> assertEquals(Direction.N, position.getDirection())
        );
    }

}

package challenge;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DirectionTest {
    @Test
    void testTurnLeft() {
        assertEquals(Direction.W, Direction.N.turnLeft());
        assertEquals(Direction.S, Direction.W.turnLeft());
    }

    @Test
    void testTurnRight() {
        assertEquals(Direction.E, Direction.N.turnRight());
        assertEquals(Direction.N, Direction.W.turnRight());
    }
}

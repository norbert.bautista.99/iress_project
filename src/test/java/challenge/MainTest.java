package challenge;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {
    @Test
    public void testRoverMovement() {
        MarsRover rover = new MarsRover(0, 0, Direction.N);
        rover.processInstructions("LMLMLMLMLM");
        assertEquals("0, 1, W", rover.toString());
    }

}

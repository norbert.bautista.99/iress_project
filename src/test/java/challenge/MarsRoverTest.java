package challenge;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MarsRoverTest {
    private MarsRover rover;

    @BeforeEach
    void setUp() {
        rover = new MarsRover(0, 0, Direction.N);
    }

    @Test
    void testProcessInstructions() {
        rover.processInstructions("LMLMLMLMLM");
        assertEquals("0, 1, W", rover.toString());
    }
}
